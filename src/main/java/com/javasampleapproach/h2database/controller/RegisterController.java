package com.javasampleapproach.h2database.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javasampleapproach.h2database.model.Customer;
import com.javasampleapproach.h2database.repository.CustomerRepository;

@Controller
@RequestMapping("/register")
public class RegisterController {
	@Autowired
	CustomerRepository rep;
	
	@RequestMapping(value = {"/"},method = RequestMethod.GET)
	public String reg(Model model) {
		
		return "register";
	}
	

	@RequestMapping(value = {"/add"}, method = RequestMethod.POST )
	@ResponseBody
	public Customer addCustomer(@RequestBody Customer c) {
		rep.save(c);
		return c;
	}
	
	
	//PUT

}
