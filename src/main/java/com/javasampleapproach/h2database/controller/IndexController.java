package com.javasampleapproach.h2database.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.javasampleapproach.h2database.model.Customer;
import com.javasampleapproach.h2database.repository.CustomerRepository;

@Controller
public class IndexController {
	@Autowired
	CustomerRepository rep;
	
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model){
		List <Customer> lista = (List<Customer>) rep.findAll();
		model.addAttribute("listaCustomer", lista);
//		Customer customer = lista.get(2);
//		model.addAttribute("id", customer.getId());
//		model.addAttribute("firstname", customer.getFirstName());
//		model.addAttribute("lastname", customer.getLastName());
		return "index";
	   }
	
	
}
