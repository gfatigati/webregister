<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/index">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/index">Home</a></li>
      <li><a href="/index">Link</a></li>
      <li><a href="/contact">Chi Siamo</a></li>
    </ul>
    <a href="/register/" class="btn btn-info  navbar-btn " role="button">Register</a>
  </div>
</nav>

<div class="container">


		<div class="input-group">
			  <input class="form-control" id="myInput" type="text" placeholder="Search..">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
	    </div>
<br>
<br>
<table class="table table-bordered table-striped">
		  <thead>
		      <tr>
		      	<th>ID</th>
		        <th>Nome</th>
		        <th>Cognome</th>
		        
		      </tr>
		  </thead>
		   <tbody id="myTable">
		    <c:forEach var="lista" items="${listaCustomer}">
		      <tr>
		        <td>${lista.id}</td>
		        <td>${lista.firstName}</td>
		        <td>${lista.lastName}</td>
		      </tr>
		    </c:forEach>
		    </tbody>
</table>
</div>
    <div class="container">
        <div class="text-right right-block">
       
                <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
	            <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
	            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
	            <a href="#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
		</div>
		</div>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

</body>
</html>