<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/index">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/index">Home</a></li>
      <li><a href="/index">Link</a></li>
      <li><a href="/contact">Chi Siamo</a></li>
    </ul>
    <a href="/register/" class="btn btn-info  navbar-btn " role="button">Register</a>
  </div>
</nav>



<div class="container">
  <h2 style="font-family:verdana;">Inserisci o Rimuovi</h2>
  <h4 style="font-family:verdana;">Inserisci Nome e Cognome per aggiungerlo alla lista</h4>
  <form>
   <div class="form-group">
  <label for="name">Nome:</label>
  <input type="text" class="form-control" id="firstName">
</div>
<div class="form-group">
  <label for="lastName">Cognome:</label>
  <input class="form-control" id="lastName">
</div>
<div class="form-group">
<button type="button" class="btn btn-primary btn-lg" id="buttonAdd">Aggiungi</button>
</div>
<br><br><br><br><br><br>
<h4 style="font-family:verdana;">Inserisci ID per eliminarlo dalla lista</h4>
<div class="form-group">
  <label for="name">ID:</label>
  <input class="form-control" id="id">
  </div>
  <div class="form-group">
<button type="button" class="btn btn-primary btn-lg" id="buttonRem">Rimuovi</button>
</div>
  </form>
</div>



<script type="text/javascript">
$(document).ready(function() {

	$("#buttonAdd").click(function(){
    var nome = $("#firstName").val();
    var cognome = $("#lastName").val();

    var Customer = {"firstName" : nome , "lastName": cognome};
    $.ajax({
      type: "POST",
  	  contentType: "application/json; charset=UTF-8",   
  	  dataType : "json", 
  	  url: "/register/add",
      data: JSON.stringify(Customer),
      
      success: function()
	      {
	    	  alert("Chiamata a buon fine...");
	      },
  

      error: function()
	      {
	        alert("Chiamata fallita, si prega di riprovare...");
	      }
    });
  });


$("#buttonRem").click(function(){
	var id = $("#id").val();
	deleteCustomer(id);
});

function deleteCustomer(id){
		$.ajax({
				    type : "DELETE",
				  	url : "/rem/" + id,
				    success: function (result) {       
				    	  alert(result + " eliminato");                
				    },
				    error: function (xhr,status,error) {
				    	alert(status+" "+error +" "+  xhr);
				    }
				});
			}

});//END READY
</script>

<div class="container">            
        <div class="text-right right-block">
       
                <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
	            <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
	            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
	            <a href="#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
		</div>
	</div>
</body>
</html>